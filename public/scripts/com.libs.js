/* Importing Modernizr */
'@import $root/public/library/modernizr/modernizr.js';

/* Importing jQuery and Friends */
'@import $root/public/library/jquery/dist/jquery.js';
'@import $root/public/library/jquery.cookie/jquery.cookie.js';
'@import $root/public/library/jquery-placeholder/jquery.placeholder.js';
'@import $root/public/library/jqpatch/dist/jqpatch.js';

/* Importing Fasclick */
'@import $root/public/library/fastclick/lib/fastclick.js';

/* Importing Foundation */
'@import $root/public/library/foundation/js/foundation.js';

/* Importing Sils IO */
'@import sails.io';