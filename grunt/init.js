/* INIT CONFIGURATIONS */
/* ------------------- */
'@namespace config';

/* Setting up environment */
if ( args.indexOf('--prod') > -1 ) {
    core.environment = 'production';
}

/* Importing Configurations */
'@import tasks/';

/* APPLYING CONFIGS */
/* ---------------- */
grunt.initConfig(config);

/* REGISTERING TASKS */
/* ----------------- */

/* Registering Default Task */
grunt.registerTask('default', []);

/* Registering Development Task */
grunt.registerTask('develop', [ 'sass', 'watch' ]);

/* Registering Default Task */
grunt.registerTask('build', []);

/* Registering Default Task */
grunt.registerTask('prod', []);

