'@namespace exports';

/* Javascript Libraries */
var libs = {
    options : {
        verbose : true
    },
    files   : {}
}

/* Application Scripts */
var apps = {
    options : {
        verbose : true
    },
    files   : {}
}

/* Adding Targets */
libs.files[ core[ 'prod-assets' ] + '/scripts' ] = core[ 'dev-assets' ] + '/scripts/com.libs.js';
apps.files[ core[ 'prod-assets' ] + '/scripts' ] = core[ 'dev-assets' ] + '/scripts/com.apps.js';

/* Loading NPM Task */
grunt.loadNpmTasks('grunt-export');