/* ============ ><><><><><><><><>< Stater ><><><><><><><><>< ============ */
/* ===========  Web Template Development is just like a Beer  =========== */
/* ============ ><><><><><><><><><><><><><><><><><><><><><>< ============ */

/* Getting Runtime Flags */
var flog = '-l ' + root + '/logs/foreverlog.log -a';

if ( args.indexOf('--debug') > -1 ) {
    flog += ' --verbose';
}
else {
    flog = '';
}

/* Configuring Watcher */
config.watch = {
    /* Grunt Watch Options */
    options : {
        livereload : core[ 'reload-port' ]
    },

    /* Static Files will reload browser directly */
    static  : {
        files : [ '!.idea', '!www', '!.tmp', '!node_modules', core[ 'dev-assets' ] + '/!(styles)', 'views/**' ]
    },

    /* Stylesheets */
    styles  : {
        files : [ '!.idea', '!www', '!.tmp', '!node_modules', core[ 'dev-assets' ] + '/styles/**/*.scss' ],
        tasks : [ 'sass:base' ]
    },

    /* Server Files will reload browser afrer reloading server */
    server  : {
        files : [ '!.idea', '!www', '!.tmp', '!node_modules', 'api/**', 'config/**', 'library/**', 'app.js' ],
        tasks : [ 'reload' ]
    },

    /* Re run Grunt */
    self    : {
        files : [ '!.idea', '!www', '!.tmp', '!node_modules', 'grunt/**', 'Gruntfile.js' ],
        tasks : [ 'regrunt' ]
    }
}

/* Loading Task */
grunt.loadNpmTasks('grunt-contrib-watch');

/* Server Reboot */
grunt.registerTask('reload', function () {
    var done = this.async();

    sudo('cd ' + root + ' && forever ' + flog + ' restart app.js ' + earg, function (err, std) {
        if ( err ) {
            console.log(err);
            done(false);
        }
        else {
            console.log(std);

            setTimeout(function () {
                done(true);
            }, core[ 'reload-wait' ] || 1500);
        }
    });
});

/* Regrunt Handler */
grunt.registerTask('regrunt', function () {
    var done = this.async();

    sudo('forever ' + flog + ' restart Gruntfile.js ' + earg, function (err, std) {
        if ( err ) {
            console.log(err);
            done(false);
        }
        else {
            console.log(std);

            setTimeout(function () {
                done(true);
            }, 200);
        }
    });
});