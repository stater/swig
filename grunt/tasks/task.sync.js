'@namespace sync';

/* Syncronize Fonts */
var fonts = {
    files           : [
        {
            cwd  : core[ 'dev-assets' ] + '/fonts',
            src  : [ '**' ],
            dest : core[ 'prod-assets' ] + '/fonts'
        }
    ],
    verbose         : true,
    updateAndDelete : true
}

/* Syncronize Images */
var images = {
    files           : [
        {
            cwd  : core[ 'dev-assets' ] + '/images',
            src  : [ '**' ],
            dest : core[ 'prod-assets' ] + '/images'
        }
    ],
    verbose         : true,
    updateAndDelete : true
}

/* Syncronize Icons */
var icons = {
    files           : [
        {
            cwd  : core[ 'dev-assets' ] + '/icons',
            src  : [ '**' ],
            dest : core[ 'prod-assets' ] + '/icons'
        }
    ],
    verbose         : true,
    updateAndDelete : true
}

/* Loading NPM Task */
grunt.loadNpmTasks('grunt-sync');