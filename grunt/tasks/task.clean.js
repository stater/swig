/* Loading NPM Task */
grunt.registerTask('clean', function () {
    var done = this.async();

    console.log('Cleaning up production location...');

    sudo('rm -Rf ' + core[ 'prod-assets' ], function (err, std) {
        if ( err ) {
            console.log(err);
            done(false);
        }
        else {
            console.log(std);
            done(true);
        }
    });
});
