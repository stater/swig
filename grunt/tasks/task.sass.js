/* ============ ><><><><><><><><>< Stater ><><><><><><><><>< ============ */
/* ===========  Web Template Development is just like a Beer  =========== */
/* ============ ><><><><><><><><><><><><><><><><><><><><><>< ============ */

'@namespace sass';

/* Comipile SCSS for Development */
var base = {
    options : {
        sourceMap   : true,
        outputStyle : 'expanded'
    },
    files   : {}
}

/* Configuring SASS by checking environment */
if ( core.environment === 'production' ) {
    base.options.outputStyle = 'compressed';
    base.files[ core[ 'prod-assets' ] + '/styles/main.css' ] = core[ 'dev-assets' ] + '/styles/prod.scss'
}
else {
    base.files[ core[ 'dev-assets' ] + '/styles/main.css' ] = core[ 'dev-assets' ] + '/styles/main.scss'
}

/* Loading NPM Task */
grunt.loadNpmTasks('grunt-sass');