/* Application Builder */
grunt.registerTask('builder', function (grunt) {
    var done = this.async();

    /* Syncing Assets */
    console.log('Syncing Assets...');

    sudo('grunt sync --prod', function (err, std) {
        if ( err ) {
            console.log(err);
            done(false);
        }
        else {
            console.log(std);
            console.log('Compiling Javascripts...');

            sudo('grunt exports --prod', function (err, std) {
                if ( err ) {
                    console.log(err);
                    done(false);
                }
                else {
                    console.log(std);
                    console.log('Compiling stylesheets...');

                    sudo('grunt sass --prod', function (err, std) {
                        if ( err ) {
                            console.log(err);
                            done(false);
                        }
                        else {
                            console.log(std);
                            done(true);
                        }
                    });
                }
            });
        }
    });
});