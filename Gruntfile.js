/* Loading NodeImport */
var imports = require('node-import'),
    sudo    = require('child_process').exec;

/* Getting CLI Commands */
var cliArg = process.argv;

/* Run development mode if command is develop */
if ( cliArg.length >= 3 && cliArg[ 2 ] === '--develop' ) {
    sudo('grunt develop ' + cliArg.slice(3).join(' '), function (err, std) {
        if ( err ) {
            console.log(err);
        }
        else {
            console.log(std);
        }
    });
}
else {
    /* Exporting Module */
    module.exports = function (grunt) {
        require('time-grunt')(grunt);

        /* Importing Configurations */
        imports.module('./grunt/init', {
            grunt : grunt,
            core  : require('./config/sailsctl'),
            root  : __dirname,
            args  : cliArg,
            earg  : cliArg.slice(3).join(' '),
            sudo  : sudo,
            impo  : imports
        });
    }
}
