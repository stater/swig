/* Routing Tables */
var RoutingTable = {};

/**
 * Adding router to Routing Tables
 * @param name - Router name
 * @param path - Router path
 * @param option - Router options
 */
var router = function (name, path, option) {
    RoutingTable[ name ] = {
        path   : path,
        option : option || false
    }
}

/* ADD ROUTER HERE */
/* --------------- */
/* Home Page */
router('home', '/', { view : 'index' });

/* Maintenance */
router('maintenance', '/lifted-down', { view : '503' });

/* Exporting Routing Tables */
module.exports = function (name) {
    if ( name === 'ALL' ) {
        return RoutingTable;
    }
    else {
        if ( name in RoutingTable ) {
            return RoutingTable[ name ].path
                .replace(/^get\s?/, '')
                .replace(/^put\s?/, '')
                .replace(/^post\s?/, '');
        }
        else {
            return '#';
        }
    }
};
