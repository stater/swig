/**
 * Swig Template Rendering Engine.
 */

module.exports = {
    /* Template File Extension */
    ext : 'swig',

    /* Function to handle render request */
    fn  : function (path, data, cb) {
        /* Binding Configurations */
        data.config = data.sails.config;

        /* Swig Renderer */
        var swig = require('swig'), min = '.min';

        if ( data.config.environment === 'development' ) {
            swig.setDefaults({ cache : false })
        }

        /* Render Templates */
        return swig.renderFile(path, data, cb);
    }
}