/**
 * SAILS CONTROL CONFIGURATIONS.
 */

/* Loading NativeJS */
if ( !GLOBAL[ 'isObject' ] ) {
    require('native-js');
}

/* Configuration Constructor */
var Config = function (obj) {
    if ( 'object' === typeof obj ) {
        for ( var key in obj ) {
            this[ key ] = obj[ key ];
        }
    }

    return this;
}

/* Configuration Prototypes */
Config.prototype = {
    /* Object Getter */
    get : function (path) {
        var arrConfig, curConfig = this, missing = false;

        if ( 'string' === typeof path ) {
            arrConfig = path.split('.');

            arrConfig.forEach(function (pn, i) {
                if ( curConfig[ pn ] ) {
                    curConfig = curConfig[ pn ];
                }
                else {
                    missing = true;
                }
            });
        }

        if ( missing ) return undefined;

        return curConfig;
    },

    /* Object Setter */
    set : function (path, value) {
        var arrConfig, curConfig = this;

        if ( 'string' == typeof path ) {
            arrConfig = path.split('.');

            arrConfig.forEach(function (pn, i) {
                if ( i === (arrConfig.length - 1) ) {
                    curConfig[ pn ] = value;
                }
                else {
                    if ( curConfig[ pn ] ) {
                        curConfig = curConfig[ pn ];
                    }
                    else {
                        curConfig[ pn ] = {};
                        curConfig = curConfig[ pn ];
                    }
                }
            });
        }

        return curConfig;
    }
}

/* Locking Configuration Prototypes */
Object.defineProperty(Config.prototype, 'get', {
    enumerable   : false,
    writable     : false,
    configurable : false
});

Object.defineProperty(Config.prototype, 'set', {
    enumerable   : false,
    writable     : false,
    configurable : false
});

/* Getting Configurations from JSON object */
var config = new Config({
    'host'        : 'localhost',
    'port'        : 1237,
    'environment' : 'development',
    'db-driver'   : 'localDiskDb',

    'dev-assets'  : 'public',
    'prod-assets' : 'www',

    'cache-age'   : 6000000,
    'reload-port' : 1437,
    'reload-wait' : 2000,

    'maintenance' : [],
    'maintenView' : '503.swig',
    'hold-update' : false,

    'statics' : []
});

try {
    var cfg = require('../configs.json');

    if ( isObject(cfg) ) {
        foreach(cfg, function (key, value) {
            config[ key ] = value;
        });
    }
}
catch ( er ) {}

/* Global Configurations */
var globs = new Config();

try {
    var cfg = require('./sailsets.json');

    if ( isObject(cfg) ) {
        foreach(cfg, function (key, value) {
            globs[ key ] = value;
        });
    }
}
catch ( e ) {}

/* Sails Control Middleware */
var sailsCtl = {
    /* Bind The User Configs */
    $config : config,
    $global : globs,

    /* Use express middleware to do more things */
    http    : {
        /* Static Folder Custom Middleware */
        customMiddleware : function (app) {
            var express = require('sails/node_modules/express'), env = sails.config.environment;

            /* Default Setatic Folder */
            if ( env === 'development' ) {
                app.use(express.static((config[ 'dev-assets' ] || 'public')));
            }
            else if ( env === 'production' ) {
                app.use(express.static((config[ 'prod-assets' ] || 'www'), { maxAge : (config[ 'cache-age' ] || 600000) }));
            }

            /* Registering Custom Static Folders */
            foreach(config.statics, function (path) {
                if ( path !== '' ) {
                    if ( env === 'development' ) {
                        app.use(express.static(path));
                    }
                    else if ( env === 'production' ) {
                        app.use(express.static(path), { maxAge : config[ 'cache-age' ] });
                    }
                }
            });
        },

        /* Controlling Middleware */
        middleware       : {
            order     : [
                'startRequestTimer',
                'cookieParser',
                'session',
                'myRequestLogger',
                'bodyParser',
                'handleBodyParserError',
                'compress',
                'methodOverride',
                'poweredBy',
                'maintener',
                '$custom',
                'router',
                'www',
                'favicon',
                '404',
                '500'
            ],

            /* Maintenance Control */
            maintener : function (req, res, next) {
                var swig = require('swig');

                if ( config.maintenance.indexOf(req.path) > -1 ) {
                    swig.renderFile('./views/' + config.maintenView, {
                        reqPath : req.path
                    }, function (err, html) {
                        if ( err ) {
                            if ( config.environment === 'development' ) {
                                console.log(err);
                            }

                            res.status(500);
                            res.send('Internal Server Error!');
                        }
                        else {
                            res.status(503);
                            res.send(html);
                        }
                    });
                }
                else {
                    next();
                }
            }
        },
    }
};

/* Applying Configs */
if ( isObject(config) ) {
    foreach(config, function (key, value) {
        sailsCtl[ key ] = value;
    });
}

/* Applying Global Configs */
if ( isObject(globs) ) {
    foreach(globs, function (key, value) {
        sailsCtl[ key ] = value;
    });
}

/* Exporting Modules */
module.exports = sailsCtl;